\section*{Teoria dos Jogos}

A emergência da cooperação é um fenômeno recorrente na teoria da evolução,
contudo, ainda não compreendemos completamente como comportamentos cooperativos
surgem  espontaneamente. Uma situação fascinante ocorre quando a cooperação
entre  indivíduos emerge de cenários conflituosos, nos quais a competição é a
melhor estratégia \cite{Axelrod1981}.

Comportamentos cooperativos e conflitosos que ocorrem entre pessoas (agentes)
são então modelado por jogos. Devido a suas regras simples, a \textit{Teoria de
Jogos} se sobressai dentre as demais por modelar situações onde o  comportamento
individualista, menos vantajoso, predomina sobre o cooperativo. A simplicidade e
riqueza dos resultados, permite que a \textit{Teoria de Jogos} seja estudada e
aplicada à diversas áreas do conhecimento como economia, biologia,  ciências
políticas e ciências da computação etc.

Um jogo é um conjunto finito de regras capaz de explicar diferentes cenários
decorrentes de comportamentos cooperativos-conflituosos entre jogadores,
agentes, pessoas, empresas etc. Tomadas de decisões (rodadas) que relacionam
conjuntos ações (estratégias) tomadas por $n$ agentes (jogadores) a um conjunto
finito de cenários (resultados) \cite{VonNeumann1944, Nash1950, Axelrod1997}.

Um \textit{jogo puro} é a definição de um jogo restrito a uma única rodada. Os
jogadores, as estratégias e as decisões tornam-se restritos a essa rodada.
Situações de cooperação-conflito de uma única rodada dificilmente são
observadas na teoria da evolução. Toma-se então uma sucessão de \textit{jogos
puros} para formar os \textit{jogos iterados}. Uma propriedade desses jogos é o
conceito de \textit{comportamento} de um jogador, definido como o conjunto de
todas as estratégias utilizadas durante todas as rodadas\cite{Gintis2000,
Pereira2008, Pereira2008a}. A estatística dos \textit{jogos iterados} então fornece as
propriedades do sistema. Para facilitar a sua construção e análise, um jogo é
dividido em três partes, \textit{jogadores}, \textit{estratégias} e
\textit{deduções} \cite{Szabo2007}.

Os \textit{jogadores} formam a primeira parte a ser definida, podem variar em
número, de $2$ a $n$ jogadores. Considera-se como \textit{racional}, um jogador
que \textit{sempre} toma suas decisões baseadas:  \textit{(i)} na maximização do
lucro individual;\textit{(ii)} na minimização dos riscos;\textit{(iii)} no
completo conhecimento do impacto de suas deduções; e \textit{(iv)} na ciência de
que outros jogadores também são \textit{racionais}. Caso um dos items anteriores
não seja satisfeito o jogador é então considerado \textit{irracional}.

A segunda parte envolve as \textit{estratégias}. Essas representam as $m$ ações,
decisões disponíveis ao $n$-ésimo jogador, formando um conjunto finito $ s_{n} =
\left\{ s^{(1)}_{n}, s^{(2)}_{n}, \dots, s^{(m)}_{n} \right\} $. Ao introduzir
\textit{jogos iterados}, uma nova classificação de estratégias torna-se
necessária. Estratégias definidas no ínicio de cada rodada são chamadas
\textit{estratégias puras}. Enquanto que, \textit{estratégias mistas} são
aquelas que utilizam resultados de anteriores (memória) e distribuição
de probabilidades adicionais. Para que não modifiquem o jogo, as
\textit{estratégias mistas} devem satisfazer a \textit{hipótese de
redutibilidade}; \textit{``Uma estratégia mista correseponde a um conjunto
estratégias puras disponiveis a um jogador"} \cite{Nash1951}.

As \textit{deduções} compõem a última parte a ser definida. Correspondem a
espectativas dos jogadores, motivando na escolha da estratégia. Equivale a um
mapa que relaciona o conjunto de estratégias aos diferentes resultados, $ P(
s^{(i)}_{n}, s^{(j)}_{n'} ) = p_{ij} $. O conjunto completo das $(m \times m)$
combinações de resultados formam a chamada \textit{matriz de resultados}.

Ao se observar comportamento entre pessoas, o conceito de estratégias
\textit{dominantes} emerge naturalmente. A capacidade de predizer  quais
estratégias predominam em um jogo torna-se então o principal objetivo para a
\textit{Teoria dos Jogos}. Uma estratégia $s^{(k)}_{n}$ é dita
\textit{dominante} sobre as demais $s^ {(i)}_{n}$ se essa fornece o maior
resultado de uma rodada; $ P_{t}(s^{(k)}_{n},s^{(k)}_{n'}) >
P_{t}(s^{(i)}_{n},s^{(i)}_{n'} ) $. Em outras palavras, uma estratégia
\textit{dominante} é aquela que leva um jogador a obter o maior resultado dentre
todas as combinações de estratégias; $s^{(k)}_{n} \rightarrow
\max{\left[P_{t}(s_{n},s_{n'})\right]}=p_{kk}$\cite{Gintis2000}. A extenção de
\textit{estratégia dominante} para mútiplos jogadores \textit{racionais} e
\textit{jogos iterados} nos leva ao chamado \textit{Equilíbrio de Nash}. Uma
estratégia dominante $s^{(j)}_{n}$ cujo o resultado
$p_{t}(s^{(j)}_{n},s^{(j)}_{n'})$ retorna para \textit{todos} os jogadores e
rodadas $ p_{t+1}, p_{t+2},\dots$, o maior resultado possível,
$P_{t+1}(s^{(k)}_{n},s^{(k)}_{n'}) = P_{t}(s^{(k)}_{n},s^{(k)}_{n'}) >
P_{t}(s^{(j)}_{n},s^{(j)}_{n'})$ é chamada de \textit{Equilíbrio de Nash}
(\textit{NE}). No \textit{NE}, os jogadores não possuem motivos para mudar de
estratégia visto que a atual sempre retorna o maior resultado, gerando um
equilíbrio estável\cite{Nash1979, Nash1950}.

% \subsection*{\normalsize Jogos}

Ainda que o \textit{NE} seja um marco fundamental, a \textit{Teoria dos Jogos}
não dispõem de uma fórmula analítica que calcule quais estratégias levam ao
\textit{NE}. No jogo, \textit{``Batalha dos Sexos"}, um casal deve escolher um
restaurante para um encontro. A escolha deve ser feita individualmente entre o
restaurante $A$ ou $B$. Nesse jogo de \textit{coordenação}  caso não haja
consenso o encontro não ocorrerá, pior resultado para ambos. O dilema surge
devido, uma preferência individual do restaurante $A$($B$) pela(o)
esposa(marido), uma \textit{assimétria} que leva ambos ao desencontro. A ecolha
pelo restaurante favorito leva o \textit{NE}\cite{Szabo2007, Gintis2000}.

Apesar dos jogos de multidões não possuírem uma representação matricial, eles
não são menos interessantes. No jogo do \textit{Bem Público} ($BP$), ($n$)
jogadores optam por investir ou não em um fundo público. Juros sobre o total
investido elevam o montante, que em seguida é compartilhado igualmente entre
todos os jogadores, incluindo os que não investiram. O paradigma levantado é:
``não investir maximiza o lucro individual porém reduz o lucro coletivo
"\cite{Szolnoki2010,  Brandt2003, Brandt2006}.

Da variedade de cenários do \textit{BP} originam-se diferentes jogos. O jogo de
\textit{somas-nulas}, ou seja, jogo como um número fixo de dinheiro é obtido
escolhendo juros nulos. Outra derivação do \textit{BP} é o \textit{Dilema do
Prisoneiro}. Essa varição consiste em um jogo com dois jogadores. No jogo do
\textit{Dilema do Prisoneiro} (\textit{DP}) dois jogadores possuem a opção de
cooperar ou delatar. Combinações destas estratégias geram três distintos
resultados; ambos cooperam, ambos delatam ou um coopera  enquanto o outro o
delata e vice-versa. Apesar da cooperação gerar o melhor resultado, o risco
envolvido gera um dilema; ``A delação miniminiza os riscos dos jogadores
\textit{racionais}". O \textit{Equilíbrio de Nash} no \textit{DP} é a
delação\cite{JohnF.Nash1979}.

% Em 1980, um torneio do ``Dilema do Prisoneiro iterado" foi organizado, pelo
% professor de ciências politicas Robert Axelrod, para determinar quais
% estratégias levam à cooperação\cite{Axelrod1981}. Dentre todas as estratégias
% apresentadas a \textit {``Tit-for-Tat"} (TFT) foi a única que gerou o
% \textit{Equilíbrio de Nash}\cite{Axelrod1981}. Proposta por Anatol Rapoport, a
% \textit{``Tit-for- Tat"} define o comportamento de um jogador como ``repetir a
% última estratégia do adversário". Com ambos jogadores utilizando a TFT o jogo
% pode convergir para um equilíbrio de cooperação. É importante resaltar que, na
% presença de outra estratégia, e.g. ``aleatória", à TFT não leva a
% cooperação\cite{Nowak1992}.
