\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{NSF}
\LoadClass[11pt, a4paper, onecolumn]{article}
%\LoadClass[12pt, a4paper, timesnewroman, onecolumn]{article}

\RequirePackage[margin=1in]{geometry}
\RequirePackage[pdftex,colorlinks,linkcolor=black,citecolor=black,urlcolor=black,filecolor=black]{hyperref}

\RequirePackage[pdftex]{graphicx}
\DeclareGraphicsExtensions{.eps,.pdf,.jpeg,.png}

\RequirePackage{fancyhdr}
\pagestyle{fancyplain}
\renewcommand{\headrulewidth}{0pt}
\fancyhf{}
\fancyfoot[R]{\thepage} 

\RequirePackage{titlecaps}
\RequirePackage[explicit]{titlesec}
%\titleformat{\section}{\large\bfseries\filcenter}{}{0pt}{\titlecap{#1}\\\rule{\textwidth}{0.4pt}}
\titleformat{\section}{\large\bfseries\filcenter}{}{0pt}{ \fontfamily{lmss}\selectfont {#1}\\\rule{\textwidth}{0.4pt}}
\titleformat{\subsection}{\bfseries}{}{0pt}{ \fontfamily{lmss}\selectfont {#1}}
%\titlespacing*{\section}{0pt}{0.5em}{0.3pt}
%\titlespacing*{\subsection}{0pt}{0.35em}{0pt}
%\titlespacing*{\subsubsection}{0pt}{0.25em}{0pt}

%\renewcommand{\title}[2]{\begin{center} {\large\bfseries{#1}} {#2}
%\date{\today} \end{center}}
\renewcommand{\title}[1]{\begin{center}{  \fontfamily{lmss}\selectfont  \large\bfseries{#1}}\end{center}}
\renewcommand{\author}[1]{\begin{center}{#1}\end{center}}
\newcommand{\address}[1]{\begin{center}\footnotesize{#1}\end{center}}


% Reset page numbering to 1.  This is helpful, since the text can only
% be 15 pages (unless otherwise specified, see individual solicitations), 
% and reviewers will want to believe we've kept it within those limits
\newcommand{\newsection}[1]{\pagenumbering{arabic}\renewcommand{\thepage}{#1--\arabic{page}}}