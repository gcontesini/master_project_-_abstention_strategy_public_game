
TEXPROJECT=main
BACKUP_FILE=backup_tex_$(shell date +%F).tar.gz 
TEXFILE=$(TEXPROJECT).tex
AUXFILE=$(TEXPROJECT).aux
BIBFILE=bibdatabase.bib
LATEX=pdflatex
# LANG=en
LANG=pt_BR

all: $(TEXFILE)
	pdflatex $(TEXFILE)
	sleep 1
	bibtex $(AUXFILE)
	sleep 1
	pdflatex $(TEXFILE)
	# make clean

clean:
	rm -rf *.toc *.log *.blg *.fls *.latexmake *.fdb_latexmk *.dvi

backup:
	tar -czvf old/$(BACKUP_FILE) *.tex *.tm *.pdf *.eps

count:
	pdftotext $(TEXPROJECT).pdf - | wc -w

aspell:
	aspell -t -c $(TEXPROJECT).tex --encoding=utf-8 --lang=pt_BR